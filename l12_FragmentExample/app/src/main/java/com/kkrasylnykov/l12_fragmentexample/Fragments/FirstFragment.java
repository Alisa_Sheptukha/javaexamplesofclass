package com.kkrasylnykov.l12_fragmentexample.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kkrasylnykov.l12_fragmentexample.Activites.MainActivity;
import com.kkrasylnykov.l12_fragmentexample.R;

public class FirstFragment extends Fragment implements View.OnClickListener {
    private Button m_button1 = null;
    private Button m_button2 = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmet_first,null);
        m_button1 = (Button) view.findViewById(R.id.F1B1);
        m_button1.setOnClickListener(this);

        m_button2 = (Button) view.findViewById(R.id.F1B2);
        m_button2.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.F1B1:
                getCurrentActtivity().showSecondFragment("Click First Button");
                break;
            case R.id.F1B2:
                getCurrentActtivity().showSecondFragment("Click Second Button");
                break;
        }
    }

    public MainActivity getCurrentActtivity(){
        return (MainActivity) getActivity();
    }
}
