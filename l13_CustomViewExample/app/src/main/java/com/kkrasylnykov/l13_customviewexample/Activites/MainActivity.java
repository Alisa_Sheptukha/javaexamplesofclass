package com.kkrasylnykov.l13_customviewexample.Activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l13_customviewexample.CustomViews.ComentsCustomView;
import com.kkrasylnykov.l13_customviewexample.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ComentsCustomView comentsCustomView = (ComentsCustomView) findViewById(R.id.customView);
        comentsCustomView.setData(R.mipmap.ic_launcher,"Test string 124124 sgsgd xcbsg sgsdg ssgs ssgsg sgsg sgsgs gsfg sfgsg sgsg");
    }
}
