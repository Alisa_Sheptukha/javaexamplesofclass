package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Activites;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Engines.UserInfoEngine;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.R;

public class ComposerActivity extends AppCompatActivity implements View.OnClickListener {

    EditText m_Name = null;
    EditText m_SName = null;
    EditText m_Phone = null;
    EditText m_Address = null;
    EditText m_BYear = null;

    Button m_addButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_composer);

        m_Name = (EditText) findViewById(R.id.edName);
        m_SName = (EditText) findViewById(R.id.edSName);
        m_Phone = (EditText) findViewById(R.id.edPhone);
        m_Address = (EditText) findViewById(R.id.edAddress);
        m_BYear = (EditText) findViewById(R.id.edBYear);

        m_addButton = (Button) findViewById(R.id.AddButton);
        m_addButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.AddButton:
                String strName = m_Name.getText().toString();
                String strSName = m_SName.getText().toString();
                String strPhone = m_Phone.getText().toString();
                String strAddress = m_Address.getText().toString();
                String strBYear = m_BYear.getText().toString();

                if (strName.isEmpty() || strSName.isEmpty() || strPhone.isEmpty()
                        || strAddress.isEmpty() || strBYear.isEmpty()){
                    Toast.makeText(this,"Не все поля заполненны!",Toast.LENGTH_LONG).show();
                    return;
                }

                int nBYear = -1;
                try {
                    nBYear = Integer.parseInt(strBYear);
                } catch (NumberFormatException e){
                    Toast.makeText(this,"Введен не корректный год рождения!",Toast.LENGTH_LONG).show();
                    return;
                }

                UserInfo info = new UserInfo(strName, strSName, strPhone, strAddress, nBYear);
                UserInfoEngine engine = new UserInfoEngine(this);
                engine.insert(info);

                m_Name.setText("");
                m_Name.requestFocus();
                m_SName.setText("");
                m_Phone.setText("");
                m_Address.setText("");
                m_BYear.setText("");

                break;
        }
    }
}
