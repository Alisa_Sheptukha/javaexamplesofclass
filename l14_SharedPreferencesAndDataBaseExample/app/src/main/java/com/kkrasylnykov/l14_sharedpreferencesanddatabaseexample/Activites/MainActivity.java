package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.CustomViews.ItemCustomView;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Engines.UserInfoEngine;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.R;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppSettings;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText m_searchEditText = null;
    Button m_addButton = null;
    LinearLayout m_contaynerLinearLayout = null;

    String m_strSearch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(AppSettings.getIsFirstStart(this)){
            AppSettings.setIsFirstStart(this, false);
            Toast.makeText(this,"This first start APP!!!!",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this,"This NOT first start APP!!!!",Toast.LENGTH_LONG).show();
        }

        m_addButton = (Button) findViewById(R.id.AddInfoButton);
        m_addButton.setOnClickListener(this);
        Button removeAllInfoButton = (Button) findViewById(R.id.RemoveAllInfoButton);
        removeAllInfoButton.setOnClickListener(this);
        m_searchEditText = (EditText) findViewById(R.id.SearchEditText);
        m_searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSearch = s.toString();
                showList();
            }
        });
        m_contaynerLinearLayout = (LinearLayout) findViewById(R.id.ConteynerLinearLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showList();
    }

    private void showList(){
        m_contaynerLinearLayout.removeAllViews();
        UserInfoEngine engine = new UserInfoEngine(this);
        ArrayList<UserInfo> arrData = null;
        if(m_strSearch.isEmpty()){
            arrData = engine.getAll();
        } else {
            arrData = engine.getBySearchString(m_strSearch);
        }
        for (UserInfo info:arrData){
            ItemCustomView item = new ItemCustomView(this,info);
            m_contaynerLinearLayout.addView(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.AddInfoButton:
                Intent intent = new Intent(this, ComposerActivity.class);
                startActivity(intent);
                break;
            case R.id.RemoveAllInfoButton:
                UserInfoEngine engine = new UserInfoEngine(this);
                engine.removeAll();
                showList();
                break;
        }
    }
}
