package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.CustomViews;


import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.R;


public class ItemCustomView extends LinearLayout {

    private TextView m_fullNameTextView = null;
    private TextView m_phoneTextView = null;
    private TextView m_addressTextView = null;


    public ItemCustomView(Context context, UserInfo info) {
        super(context);
        initView(context);
        m_fullNameTextView.setText(info.getFullName());
        m_phoneTextView.setText(info.getPhone());
        m_addressTextView.setText(info.getAddress());
    }

    public ItemCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ItemCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context){
        LayoutInflater.from(context).inflate(R.layout.custom_view_coments,this,true);
        setOrientation(VERTICAL);
        m_fullNameTextView = (TextView) findViewById(R.id.textFullName);
        m_phoneTextView = (TextView) findViewById(R.id.textPhone);
        m_addressTextView = (TextView) findViewById(R.id.textAddress);
    }
}
