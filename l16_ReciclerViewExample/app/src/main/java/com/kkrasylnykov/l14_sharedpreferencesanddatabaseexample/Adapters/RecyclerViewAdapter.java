package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.R;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_BUTTOM_ITEM = 1;


    private ArrayList<UserInfo> m_arrData = null;

    private OnClickItemInReciclerViewListener m_listener = null;

    public RecyclerViewAdapter(ArrayList<UserInfo> arrData) {
        m_arrData = arrData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder newHolder = null;
        if (viewType==TYPE_ITEM){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_view_coments, parent, false);
            newHolder = new RecordInNotepadHolder(v);
        } else if (viewType==TYPE_BUTTOM_ITEM){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_buttom, parent, false);
            newHolder = new ButtomItemHolder(v);
        }

        return newHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int nType = getItemViewType(position);
        if (nType==TYPE_ITEM){
            UserInfo info = m_arrData.get(position);
            RecordInNotepadHolder recordHolder = (RecordInNotepadHolder) holder;
            recordHolder.nId = info.getId();
            recordHolder.fullNameTextView.setText(info.getFullName());
            recordHolder.phoneTextView.setText(info.getPhone());
            recordHolder.addressTextView.setText(info.getAddress());
        } else {
            ButtomItemHolder buttomItemHolder = (ButtomItemHolder) holder;
            buttomItemHolder.infoTextView.setText("Это последний элемент в списке");
        }

    }

    @Override
    public int getItemCount() {
        return m_arrData.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        int nType = TYPE_ITEM;
        if (position==m_arrData.size()){
            nType = TYPE_BUTTOM_ITEM;
        }
        return nType;
    }

    public void setOnClickListener(OnClickItemInReciclerViewListener listener){
        m_listener = listener;
    }

    public class RecordInNotepadHolder extends RecyclerView.ViewHolder {

        long nId = -1;

        TextView fullNameTextView = null;
        TextView phoneTextView = null;
        TextView addressTextView = null;

        public RecordInNotepadHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (m_listener!=null){
                        m_listener.onClickItemInReciclerView(nId);
                    }
                }
            });
            fullNameTextView = (TextView) itemView.findViewById(R.id.textFullName);
            phoneTextView = (TextView) itemView.findViewById(R.id.textPhone);
            addressTextView = (TextView) itemView.findViewById(R.id.textAddress);

        }
    }

    public class ButtomItemHolder extends RecyclerView.ViewHolder {

        TextView infoTextView = null;

        public ButtomItemHolder(View itemView) {
            super(itemView);
            infoTextView = (TextView) itemView.findViewById(R.id.textButtomItem);

        }
    }

    public interface OnClickItemInReciclerViewListener{
        public abstract void onClickItemInReciclerView(long nId);
    }
}
