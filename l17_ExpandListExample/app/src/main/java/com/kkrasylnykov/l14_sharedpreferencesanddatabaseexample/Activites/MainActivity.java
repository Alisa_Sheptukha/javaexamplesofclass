package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Adapters.NotepadExpandableAdapter;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Engines.UserInfoEngine;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.R;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppSettings;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NotepadExpandableAdapter.OnClickGroupListener {

    EditText m_searchEditText = null;
    Button m_addButton = null;
    ExpandableListView m_ExpandableListView = null;

    NotepadExpandableAdapter m_adapter = null;

    ArrayList<UserInfo> m_arrData = new ArrayList<>();
    String m_strSearch = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(AppSettings.getIsFirstStart(this)){
            AppSettings.setIsFirstStart(this, false);
            Toast.makeText(this,"This first start APP!!!!",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this,"This NOT first start APP!!!!",Toast.LENGTH_LONG).show();
        }

        m_addButton = (Button) findViewById(R.id.AddInfoButton);
        m_addButton.setOnClickListener(this);
        Button removeAllInfoButton = (Button) findViewById(R.id.RemoveAllInfoButton);
        removeAllInfoButton.setOnClickListener(this);
        m_searchEditText = (EditText) findViewById(R.id.SearchEditText);
        m_searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSearch = s.toString();
                showList();
            }
        });
        m_ExpandableListView = (ExpandableListView) findViewById(R.id.mainExpandableListView);
        m_adapter = new NotepadExpandableAdapter(m_arrData);
        m_ExpandableListView.setAdapter(m_adapter);
        m_adapter.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showList();
    }

    private void showList(){
        UserInfoEngine engine = new UserInfoEngine(this);
        m_arrData.clear();
        if(m_strSearch.isEmpty()){
            m_arrData.addAll(engine.getAll());
        } else {
            m_arrData.addAll(engine.getBySearchString(m_strSearch));
        }
        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.AddInfoButton:
                Intent intent = new Intent(this, ComposerActivity.class);
                startActivity(intent);
                break;
            case R.id.RemoveAllInfoButton:
                UserInfoEngine engine = new UserInfoEngine(this);
                engine.removeAll();
                showList();
                break;
        }
    }

    @Override
    public void onClickGroup(long nId) {
        Intent intent = new Intent(this, ComposerActivity.class);
        intent.putExtra(ComposerActivity.KEY_RECORD_ID, nId);
        startActivity(intent);
    }
}
