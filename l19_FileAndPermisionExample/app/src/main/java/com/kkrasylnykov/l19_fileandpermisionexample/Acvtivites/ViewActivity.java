package com.kkrasylnykov.l19_fileandpermisionexample.Acvtivites;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.kkrasylnykov.l19_fileandpermisionexample.R;

import java.io.File;

public class ViewActivity extends AppCompatActivity {
    public final static String KEY_FILE_PATH = "KEY_FILE_PATH";

    ImageView m_imageView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        m_imageView = (ImageView) findViewById(R.id.ViewImageView);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                String strPath = bundle.getString(KEY_FILE_PATH, "");
                if (!strPath.isEmpty()){
                    File file = new File(strPath);
                    if (file.exists()){
                        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),bmOptions);
                        m_imageView.setImageBitmap(bitmap);
                    }
                }
            }
        }
    }
}
