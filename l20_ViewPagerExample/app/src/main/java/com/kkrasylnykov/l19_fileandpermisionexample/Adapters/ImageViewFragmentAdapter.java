package com.kkrasylnykov.l19_fileandpermisionexample.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kkrasylnykov.l19_fileandpermisionexample.Fragments.ImageFragment;

public class ImageViewFragmentAdapter extends FragmentPagerAdapter {

    String[] m_arrPaths = null;

    public ImageViewFragmentAdapter(FragmentManager fm, String[] arrPaths) {
        super(fm);
        m_arrPaths = arrPaths;
    }

    @Override
    public Fragment getItem(int position) {
        ImageFragment fragment = new ImageFragment();
        fragment.setFilePath(m_arrPaths[position]);
        return fragment;
    }

    @Override
    public int getCount() {
        return m_arrPaths.length;
    }
}
