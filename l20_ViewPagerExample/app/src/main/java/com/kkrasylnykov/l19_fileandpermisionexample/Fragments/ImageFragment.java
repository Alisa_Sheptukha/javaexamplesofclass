package com.kkrasylnykov.l19_fileandpermisionexample.Fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kkrasylnykov.l19_fileandpermisionexample.R;

import java.io.File;

public class ImageFragment extends Fragment {

    String m_strFilePath = "";
    ImageView m_ImageView = null;
    Bitmap m_bitmap = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        m_ImageView = (ImageView) view.findViewById(R.id.FragmentImageView);
        return view;
    }

    public void setFilePath(String strFilePath){
        m_strFilePath = strFilePath;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (m_bitmap==null){
            if (!m_strFilePath.isEmpty()){
                File file = new File(m_strFilePath);
                if (file.exists()){
                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int nDispleyWidth = size.x;

                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;

                    BitmapFactory.decodeFile(m_strFilePath, bmOptions);

                    int nFileWidth = bmOptions.outWidth;

                    float fScale = (float)nDispleyWidth/(float)nFileWidth;
                    int nScale = (nFileWidth/nDispleyWidth);


                    bmOptions = new BitmapFactory.Options();
                    bmOptions.inSampleSize =nScale;
                    m_bitmap = BitmapFactory.decodeFile(m_strFilePath,bmOptions);
                    m_ImageView.setImageBitmap(m_bitmap);
                }
            }
        } else {
            m_ImageView.setImageBitmap(m_bitmap);
        }

    }
}
