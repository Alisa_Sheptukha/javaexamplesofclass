package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Wrappers.DBWrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppConstants;

import java.util.ArrayList;

public class UserInfoDBWrapper extends BaseDBWrapper {
    public UserInfoDBWrapper(Context Context) {
        super(Context, AppConstants.DB_TABLE_USERS.TABLE_NAME);
    }

    public long insert(UserInfo item){
        SQLiteDatabase db = getDBWrite();
        long nResult = db.insert(getTableName(),null,item.getContentValues());
        db.close();
        return nResult;

    }

    public void remove(UserInfo item){
        SQLiteDatabase db = getDBWrite();
        db.delete(getTableName(), AppConstants.DB_TABLE_USERS.FIELD_ID + "=?",new String[]{Long.toString(item.getId())});
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getDBWrite();
        db.delete(getTableName(), null, null);
        db.close();
    }

    public void update(UserInfo item){
        SQLiteDatabase db = getDBWrite();
        db.update(getTableName(),item.getContentValues(), AppConstants.DB_TABLE_USERS.FIELD_ID + "=?",new String[]{Long.toString(item.getId())});
        db.close();
    }

    public ArrayList<UserInfo> getAll(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getDBRead();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null && cursor.moveToFirst()){
            do{
                arrResult.add(new UserInfo(cursor));
            } while(cursor.moveToNext());

        }
        cursor.close();
        db.close();
        return arrResult;
    }

    public UserInfo getById(long nId){
        UserInfo result = null;
        SQLiteDatabase db = getDBRead();
        String strQuery = AppConstants.DB_TABLE_USERS.FIELD_ID + "=?";
        String[] arrParam = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(getTableName(), null, strQuery, arrParam, null, null, null);

        if (cursor!=null && cursor.moveToFirst()){
            result = new UserInfo(cursor);

        }
        cursor.close();
        db.close();

        return result;
    }

    public ArrayList<UserInfo> getBySearchString(String strSearch){
        strSearch = strSearch + "%";
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getDBRead();
        String strQuery = AppConstants.DB_TABLE_USERS.FIELD_NAME + " LIKE ? OR " +
                AppConstants.DB_TABLE_USERS.FIELD_SNAME + " LIKE ?";
        String[] arrParam = new String[]{strSearch,strSearch,strSearch};
        Cursor cursor = db.query(getTableName(), null, strQuery, arrParam, null, null, null);

        if (cursor!=null && cursor.moveToFirst()){
            do{
                arrResult.add(new UserInfo(cursor));
            } while(cursor.moveToNext());

        }
        cursor.close();
        db.close();
        return arrResult;
    }
}
