package com.kkrasylnykov.l22_multithreadexample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    private static  final String KEY_TYPE = "KEY_TYPE";
    private static  final String KEY_DATA = "KEY_DATA";

    ImageView m_imageView = null;
    TextView m_TextView = null;
    Bitmap m_bitmap = null;
    Handler m_handler = null;
    SuperThread m_thread = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg==null){
                    return;
                }
                Bundle bundle = msg.getData();
                if(bundle==null){
                    return;
                }
                String strType = bundle.getString(KEY_TYPE,"");
                int nData = bundle.getInt(KEY_DATA,-1);

                m_TextView.setText(strType + " -> " + nData);
            }
        };
        m_imageView = (ImageView) findViewById(R.id.ImageView);
        m_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Random rand = new Random();
                        ExecutorService executorService = Executors.newFixedThreadPool(2);
                        int nCount = 10;
                        CountDownLatch doneSignal = new CountDownLatch(nCount);
                        for (int i=0; i<nCount; i++){
                            Runnable runnable = new DownloadRunnable(i,(rand.nextInt(10)*100), rand.nextInt(10),doneSignal);
                            executorService.execute(runnable);
                        }

                        try {
                            doneSignal.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "All Downloads is done!!!", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                }).start();

                /*Реализация Executer*/
//                Random rand = new Random();
//                ExecutorService executorService = Executors.newFixedThreadPool(2);
//                int nCount = 20;
//                for (int i=0; i<nCount; i++){
//                    Runnable runnable = new DownloadRunnable(i,(rand.nextInt(10)*100), rand.nextInt(10));
//                    executorService.execute(runnable);
//                }
                /*Реализация Thread(парелельное выполнение)*/
//                if (m_thread!=null){
//                    return;
//                }
//                m_thread = new SuperThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        int nCount = 0;
//                        int nCountW = m_bitmap.getWidth();
//                        int nCountH = m_bitmap.getHeight();
//                        long nCountPixel = nCountW*nCountH;
//                        long nCountProcessingPixel = 0;
//                        int nOldProcent = -1;
//                        for (int i=0; i<nCountW; i++){
//                            if(!m_thread.getIsRun()){
//                                Bundle bundle = new Bundle();
//                                bundle.putString(KEY_TYPE, "STOP THREAD");
//                                bundle.putInt(KEY_DATA, nCount);
//                                Message msg = new Message();
//                                msg.setData(bundle);
//                                if(m_handler!=null){
//                                    m_handler.sendMessage(msg);
//                                }
//                                m_thread = null;
//                                return;
//                            }
//                            for (int j=0; j<nCountH; j++){
//                                if(getResources().getColor(android.R.color.white)==m_bitmap.getPixel(i,j)){
//                                    nCount ++;
//                                }
//                                nCountProcessingPixel++;
//                                int nProcent = (int)(((double)nCountProcessingPixel/(double)nCountPixel)*100);
//                                if(nOldProcent!=nProcent){
//                                    nOldProcent = nProcent;
//                                    Bundle bundle = new Bundle();
//                                    bundle.putString(KEY_TYPE, "In process");
//                                    bundle.putInt(KEY_DATA, nProcent);
//                                    Message msg = new Message();
//                                    msg.setData(bundle);
//                                    if(m_handler!=null){
//                                        m_handler.sendMessage(msg);
//                                    }
//                                }
//                            }
//                        }
//                        Log.d("devcpp12345", "nCount -> " + nCount);
//                        Bundle bundle = new Bundle();
//                        bundle.putString(KEY_TYPE, "nCount");
//                        bundle.putInt(KEY_DATA, nCount);
//                        Message msg = new Message();
//                        msg.setData(bundle);
//                        if(m_handler!=null){
//                            m_handler.sendMessage(msg);
//                        }
//                        m_thread = null;
//                        /*runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                m_TextView.setText("nCount -> " + nCountForUI);
//                            }
//                        });*/
//
//                    }
//                });
//                m_thread.start();
                /*Реализация AsyncTask (асинхронное выполнение)*/
//                AsyncTask<Integer,Integer, Integer> asynk = new AsyncTask<Integer, Integer, Integer>() {
//                    @Override
//                    protected void onPreExecute() {
//                        super.onPreExecute();
//                        Log.d("devcpp12345", "Start calculation");
//                        m_TextView.setText("Start calculation");
//                    }
//
//                    @Override
//                    protected Integer doInBackground(Integer... integers) {
//                        int nCount = 0;
//                        int nCountW = m_bitmap.getWidth();
//                        int nCountH = m_bitmap.getHeight();
//                        for (int i=0; i<nCountW; i++){
//                            for (int j=0; j<nCountH; j++){
//                                if(getResources().getColor(android.R.color.white)==m_bitmap.getPixel(i,j)){
//                                    nCount ++;
//                                }
//                            }
//                        }
//                        return nCount;
//                    }
//
//                    @Override
//                    protected void onPostExecute(Integer integer) {
//                        Log.d("devcpp12345", "nCount -> " + integer);
//                        m_TextView.setText("nCount -> " + integer);
//                    }
//                };
//
//                asynk.execute();
            }
        });

        findViewById(R.id.Button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (m_thread!=null){
                    m_thread.stopThread();
                }
                Toast.makeText(MainActivity.this, "Show text", Toast.LENGTH_LONG).show();
            }
        });


        m_TextView = (TextView) findViewById(R.id.TextView);

        String strPath = "/storage/emulated/0/Download/IMG_9793.jpg";

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        m_bitmap = BitmapFactory.decodeFile(strPath,bmOptions);
        m_imageView.setImageBitmap(m_bitmap);
    }

    public class SuperThread extends Thread{

        private boolean m_bIsRun = true;

        public SuperThread(Runnable runnable){
            super(runnable);
        }

        public void stopThread(){
            m_bIsRun = false;
        }

        public boolean getIsRun(){
            return m_bIsRun;
        }
    }

    public class DownloadRunnable implements Runnable{

        private int m_nPosition = 0;
        private long m_nTimeSleep = 0;
        private long m_nIntCount = 0;
        private CountDownLatch m_doneSignal;

        public DownloadRunnable(int nPosition, long nTimeSleep, long nIntCount, CountDownLatch doneSignal){
            m_nPosition = nPosition;
            m_nTimeSleep = nTimeSleep;
            m_nIntCount = nIntCount;
            m_doneSignal = doneSignal;
        }

        @Override
        public void run() {
            //Этот код эмулирует работу разную по времени
            Log.d("devcpp12345", "start -> " + m_nPosition);
            for (long i=0; i<m_nIntCount; i++){
                try {
                    Thread.sleep(m_nTimeSleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            m_doneSignal.countDown();
            Log.d("devcpp12345", "end -> " + m_nPosition);
        }
    }
}
