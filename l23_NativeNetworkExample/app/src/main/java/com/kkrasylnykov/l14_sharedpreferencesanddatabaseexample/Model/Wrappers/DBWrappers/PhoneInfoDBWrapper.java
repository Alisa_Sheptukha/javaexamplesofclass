package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Wrappers.DBWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.PhoneInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppConstants;

import java.util.ArrayList;

public class PhoneInfoDBWrapper extends BaseDBWrapper {

    public PhoneInfoDBWrapper(Context Context) {
        super(Context, AppConstants.DB_TABLE_PHONES.TABLE_NAME);
    }

    public void insert(PhoneInfo item){
        SQLiteDatabase db = getDBWrite();
        db.insert(getTableName(),null,item.getContentValues());
        db.close();
    }

    public void remove(PhoneInfo item){
        SQLiteDatabase db = getDBWrite();
        db.delete(getTableName(), AppConstants.DB_TABLE_PHONES.FIELD_ID + "=?",new String[]{Long.toString(item.getId())});
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getDBWrite();
        db.delete(getTableName(), null, null);
        db.close();
    }

    public void update(PhoneInfo item){
        SQLiteDatabase db = getDBWrite();
        db.update(getTableName(),item.getContentValues(), AppConstants.DB_TABLE_PHONES.FIELD_ID + "=?",new String[]{Long.toString(item.getId())});
        db.close();
    }

    public ArrayList<PhoneInfo> getPhoneByUserId(long nUserId){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        UserInfo result = null;
        SQLiteDatabase db = getDBRead();
        String strQuery = AppConstants.DB_TABLE_PHONES.FIELD_USER_ID + "=?";
        String[] arrParam = new String[]{Long.toString(nUserId)};
        Cursor cursor = db.query(getTableName(), null, strQuery, arrParam, null, null, null);

        if (cursor!=null && cursor.moveToFirst()){
            do {
                arrResult.add(new PhoneInfo(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return arrResult;
    }

}
