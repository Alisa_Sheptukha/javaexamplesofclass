package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Wrappers.NetworkWrappers;

import android.content.Context;
import android.util.Log;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class UserInfoNetworkWrapper extends BaseNetworkWrapper {
    public UserInfoNetworkWrapper(Context context) {
        super(context);
    }

    public boolean sendInsertRequest(UserInfo item){
        boolean bResult = false;
        String strURL = SERVER_NAME + "users.json";
        String strBody = item.getJSONObject().toString();

        try {
            URL url = new URL(strURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");

            byte[] postData = strBody.getBytes(StandardCharsets.UTF_8);
            connection.getOutputStream().write(postData);

            connection.connect();

            int response = connection.getResponseCode();
            if(response==SERVER_RESPONSE_OK){
                InputStream is = connection.getInputStream();
                String strResponse = "";
                if (is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }

                Log.d("devcpp12345", "strResponse -> " + strResponse);
                JSONObject jsonResponse = new JSONObject(strResponse);
                long nServerId = jsonResponse.getLong(AppConstants.JSON_USERS.FIELD_SERVER_ID);

                item.setServerId(nServerId);
                bResult = true;
            }

            return bResult;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return bResult;
    }

    public ArrayList<UserInfo> serdLoaedAllUserInfoRequest(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        String strURL = SERVER_NAME + "users.json";

        try {
            URL url = new URL(strURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("GET");
            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");

            connection.connect();

            int response = connection.getResponseCode();
            if(response==SERVER_RESPONSE_OK){
                InputStream is = connection.getInputStream();
                String strResponse = "";
                if (is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }

                Log.d("devcpp12345", "strResponse -> " + strResponse);
                JSONArray arrResponse = new JSONArray(strResponse);
                int nCount = arrResponse.length();
                for (int i=0;i<nCount;i++){
                    JSONObject jsonObj =  arrResponse.getJSONObject(i);
                    arrResult.add(new UserInfo(jsonObj));
                }

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrResult;
    }
}
