package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants;


public class AppConstants {
    public static final int DBVersion = 1;

    public static final String DBFileName = "l14_db";

    public static class DB_TABLE_USERS{
        public static final String TABLE_NAME = "UserInfo";

        public static final String FIELD_ID = "_id";
        public static final String FIELD_SERVER_ID = "server_id";
        public static final String FIELD_NAME = "_name";
        public static final String FIELD_SNAME = "_sname";
        public static final String FIELD_ADDRESS = "_address";
        public static final String FIELD_BYEAR = "_byear";

    }

    public static class JSON_USERS{
        public static final String FIELD_SERVER_ID = "id";
        public static final String FIELD_NAME = "name";
        public static final String FIELD_SNAME = "sname";
        public static final String FIELD_PHONES = "phones";
        public static final String FIELD_ADDRESS = "address";

    }

    public static class DB_TABLE_PHONES{
        public static final String TABLE_NAME = "UserPhones";

        public static final String FIELD_ID = "_id";
        public static final String FIELD_USER_ID = "_user_id";
        public static final String FIELD_PHONE = "_phone";
    }
}


