package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {


    public DbHelper(Context context) {
        super(context, AppConstants.DBFileName, null, AppConstants.DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + AppConstants.DB_TABLE_USERS.TABLE_NAME +
                "(" + AppConstants.DB_TABLE_USERS.FIELD_ID + " integer primary key autoincrement,"+
                AppConstants.DB_TABLE_USERS.FIELD_SERVER_ID + " integer, " +
                AppConstants.DB_TABLE_USERS.FIELD_NAME + " text, " +
                AppConstants.DB_TABLE_USERS.FIELD_SNAME + " text, "+
                AppConstants.DB_TABLE_USERS.FIELD_ADDRESS + " text," +
                AppConstants.DB_TABLE_USERS.FIELD_BYEAR + " integer);");

        db.execSQL("CREATE TABLE " + AppConstants.DB_TABLE_PHONES.TABLE_NAME +
                "(" + AppConstants.DB_TABLE_PHONES.FIELD_ID + " integer primary key autoincrement,"+
                AppConstants.DB_TABLE_PHONES.FIELD_USER_ID + " integer, " +
                AppConstants.DB_TABLE_PHONES.FIELD_PHONE + " text);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO IMPLEMENTING AFTER UPGRADE DB
    }

    public SQLiteDatabase getDBWrite(){
        return this.getWritableDatabase();
    }

    public SQLiteDatabase getDBRead(){
        return this.getReadableDatabase();
    }
}
