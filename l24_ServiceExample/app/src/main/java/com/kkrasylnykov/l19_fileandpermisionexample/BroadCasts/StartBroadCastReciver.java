package com.kkrasylnykov.l19_fileandpermisionexample.BroadCasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kkrasylnykov.l19_fileandpermisionexample.Services.SendStatisticsService;

public class StartBroadCastReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("devcppl19BroadCast","StartBroadCastReciver ->");
        Intent startServiceIntent = new Intent(context, SendStatisticsService.class);
        context.startService(startServiceIntent);
    }
}
