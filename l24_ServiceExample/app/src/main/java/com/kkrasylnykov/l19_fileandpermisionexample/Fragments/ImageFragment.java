package com.kkrasylnykov.l19_fileandpermisionexample.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kkrasylnykov.l19_fileandpermisionexample.Model.FileOnServer;
import com.kkrasylnykov.l19_fileandpermisionexample.R;
import com.kkrasylnykov.l19_fileandpermisionexample.Services.DownloadImageService;
import com.kkrasylnykov.l19_fileandpermisionexample.Tools.Settings;

import java.io.File;

public class ImageFragment extends Fragment {

    FileOnServer m_FileOnServer = null;
    ImageView m_ImageView = null;
    Bitmap m_bitmap = null;

    BroadcastReceiver m_BroadcastReceiver = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        m_ImageView = (ImageView) view.findViewById(R.id.FragmentImageView);
        return view;
    }

    public void setFilePath(FileOnServer fileOnServer){
        m_FileOnServer = fileOnServer;
    }

    private void loadImageView(){
        String strLocalPath  = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/" + m_FileOnServer.getLocalName();

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int nDispleyWidth = size.x;

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(strLocalPath, bmOptions);

        int nFileWidth = bmOptions.outWidth;

        float fScale = (float)nDispleyWidth/(float)nFileWidth;
        int nScale = (nFileWidth/nDispleyWidth);


        bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize =nScale;
        m_bitmap = BitmapFactory.decodeFile(strLocalPath,bmOptions);
        m_ImageView.setImageBitmap(m_bitmap);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (m_bitmap==null){
            if (m_FileOnServer!=null){
                String strLocalPath  = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/" + m_FileOnServer.getLocalName();
                File file = new File (strLocalPath);
                if (file.exists()){
                    loadImageView();
                } else {
                    m_BroadcastReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            loadImageView();
                            if (m_BroadcastReceiver!=null){
                                getActivity().unregisterReceiver(m_BroadcastReceiver);
                                m_BroadcastReceiver = null;
                            }

                        }
                    };

                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction(DownloadImageService.ACTION_DOWNLOAD+strLocalPath);
                    getActivity().registerReceiver(m_BroadcastReceiver,intentFilter);

                    Intent intent = new Intent(getActivity(), DownloadImageService.class);
                    intent.putExtra(DownloadImageService.KEY_LOCAL_PATH, strLocalPath);
                    intent.putExtra(DownloadImageService.KEY_SERVER_URL, m_FileOnServer.getServerUrl());
                    getActivity().startService(intent);
                }
            }
        } else {
            m_ImageView.setImageBitmap(m_bitmap);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (m_BroadcastReceiver!=null){
            getActivity().unregisterReceiver(m_BroadcastReceiver);
            m_BroadcastReceiver = null;
        }
    }

    public void changeCountShowImage(){
        Log.d("devcppl19App","changeCountShowImage -> ");
        int nCount = Settings.getCountShowImage(getActivity(),m_FileOnServer.getLocalName());
        nCount++;
        Settings.setCountShowImage(getActivity(), m_FileOnServer.getLocalName(), nCount);

    }
}
