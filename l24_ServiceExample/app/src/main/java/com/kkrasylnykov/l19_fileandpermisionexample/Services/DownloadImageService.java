package com.kkrasylnykov.l19_fileandpermisionexample.Services;


import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l19_fileandpermisionexample.Model.FileOnServer;
import com.kkrasylnykov.l19_fileandpermisionexample.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadImageService extends Service {

    public static final String KEY_LOCAL_PATH = "KEY_LOCAL_PATH";
    public static final String KEY_SERVER_URL = "KEY_SERVER_URL";

    public static final String ACTION_DOWNLOAD = "com.kkrasylnykov.l19_fileandpermisionexample.Services.DownloadImageService_ACTION_DOWNLOAD_";

    private static ArrayList<String> arrDownloadsFile = new ArrayList<>();
    private static ExecutorService s_ExecutorService = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent==null){
            return -1;
        }
        Bundle bundle = intent.getExtras();
        if (bundle==null){
            return -1;
        }
        String strPath = bundle.getString(KEY_LOCAL_PATH);
        if (arrDownloadsFile.contains(strPath)){
            return 0;
        }
        arrDownloadsFile.add(strPath);
        String strUrl = bundle.getString(KEY_SERVER_URL);
        FileOnServer fileOnServer = new FileOnServer(strPath, strUrl);

        Runnable runnable = new DownloadRunnable(fileOnServer);

        if(s_ExecutorService==null){
            s_ExecutorService = Executors.newFixedThreadPool(2);
        }
        s_ExecutorService.execute(runnable);
        return 0;
    }

    public class DownloadRunnable implements Runnable{

        FileOnServer m_fileOnServer = null;

        public DownloadRunnable(FileOnServer fileOnServer){
            m_fileOnServer = fileOnServer;
        }


        @Override
        public void run() {
            URL url = null;
            try {
                Log.d("devcpp", "onStartCommand -> run -> 1 ->" + m_fileOnServer.getServerUrl());
                url = new URL(m_fileOnServer.getServerUrl());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(1000);
                conn.setConnectTimeout(1000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                Log.d("devcpp", "onStartCommand -> run -> 2 ->" + m_fileOnServer.getLocalName());
                int response = conn.getResponseCode();
                if (response!=200){
                    return;
                }
                Log.d("devcpp", "RunnableGET -> response -> " + m_fileOnServer.getLocalName() + " -> " + response);
                int contentLength = conn.getContentLength();
                Log.d("devcpp", "RunnableGET -> contentLength -> " + m_fileOnServer.getLocalName() + " -> " + contentLength);
                InputStream inputStream = conn.getInputStream();
                //Создаем файл на диске
                FileOutputStream outputStream = new FileOutputStream(m_fileOnServer.getLocalName());

                int bytesRead = -1;
                byte[] buffer = new byte[4096];

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                outputStream.close();
                inputStream.close();

                Intent intent = new Intent();
                intent.setAction(ACTION_DOWNLOAD+m_fileOnServer.getLocalName());
                sendBroadcast(intent);


                Log.d("devcpp", "RunnableGET -> response -> " + m_fileOnServer.getLocalName() + " -> DOWNLOAD!!!");

            } catch (MalformedURLException e) {
                Log.e("devcpp", "MalformedURLException -> " + e.toString());
                e.printStackTrace();
            } catch (ProtocolException e) {
                Log.e("devcpp", "ProtocolException -> " + e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("devcpp", "IOException -> " + e.toString());
                e.printStackTrace();
            } catch (Exception e){
                Log.e("devcpp", "!!!Exception -> " + e.toString());
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
