package com.kkrasylnykov.l19_fileandpermisionexample.Tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Settings {

    private static final String KEY_SHOW_COUNT_IMAGE = "KEY_SHOW_COUNT_IMAGE_";


    private static SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static int getCountShowImage(Context context, String strImage){
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        int nCount = sharedPreferences.getInt(KEY_SHOW_COUNT_IMAGE + strImage,0);
        Log.d("devcppl19Settings","getCountShowImage -> " + strImage + "<<>>" + nCount);
        return nCount;
    }

    public static void setCountShowImage(Context context, String strImage, int nCount){
        Log.d("devcppl19Settings","setCountShowImage -> " + strImage + "<<>>" + nCount);
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(KEY_SHOW_COUNT_IMAGE + strImage, nCount);
        editor.commit();

    }
}
