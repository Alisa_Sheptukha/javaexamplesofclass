package com.kkrasylnykov.l19_fileandpermisionexample.Acvtivites;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kkrasylnykov.l19_fileandpermisionexample.Adapters.FileListAdapter;
import com.kkrasylnykov.l19_fileandpermisionexample.Model.FileOnServer;
import com.kkrasylnykov.l19_fileandpermisionexample.R;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final int REQUEST_PERMISSION_ON_ATTACH_FILE = 1;

    ListView m_listView = null;
    ArrayList<FileOnServer> m_arrData = null;
    FileListAdapter m_adapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Получение пути к директориям системы.
        //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_ATTACH_FILE);
        }else{
            onCreate();
        }

    }

    public void onCreate(){
        m_arrData = getFiles();
//        m_arrData = getJpgFiles(Environment.getExternalStorageDirectory());
        m_listView = (ListView) findViewById(R.id.MainListView);
        m_adapter = new FileListAdapter(m_arrData);
        m_listView.setAdapter(m_adapter);
        m_listView.setOnItemClickListener(this);
    }

    public static ArrayList<FileOnServer> getFiles(){
        ArrayList<FileOnServer> arrResult = new ArrayList<>();
        arrResult.add(new FileOnServer("file_0.png","https://pp.vk.me/c630216/v630216587/38e0d/xX2qGqsZrGE.jpg"));
        arrResult.add(new FileOnServer("file_1.png","https://pp.vk.me/c630216/v630216587/38e17/yaJqhxL1UD8.jpg"));
        arrResult.add(new FileOnServer("file_2.png","https://pp.vk.me/c630216/v630216587/38e3f/0LdLK_GtvZo.jpg"));
        arrResult.add(new FileOnServer("file_3.png","https://pp.vk.me/c630216/v630216587/38e35/N3_Q1J621zU.jpg"));
        arrResult.add(new FileOnServer("file_4.png","https://pp.vk.me/c630216/v630216587/38e49/vGE22vZUuMk.jpg"));
        return arrResult;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, ViewActivity.class);
        intent.putExtra(ViewActivity.KEY_POSITION, i);
        startActivity(intent);
    }

    public String[] arrayFilesToString(ArrayList<File> arrData){
        int nCount = arrData.size();
        String[] arrReturn = new String[nCount];
        for (int i=0; i<nCount; i++){
            arrReturn[i] = arrData.get(i).getAbsolutePath();
        }
        return arrReturn;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode==REQUEST_PERMISSION_ON_ATTACH_FILE){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(MainActivity.this, "Приложение не может работать без разрешения", Toast.LENGTH_LONG).show();
                finish();
            }else {
                onCreate();
            }
        }
    }
}
