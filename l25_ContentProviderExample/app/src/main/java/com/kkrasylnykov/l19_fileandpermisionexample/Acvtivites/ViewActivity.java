package com.kkrasylnykov.l19_fileandpermisionexample.Acvtivites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.kkrasylnykov.l19_fileandpermisionexample.Adapters.ImageViewFragmentAdapter;
import com.kkrasylnykov.l19_fileandpermisionexample.Fragments.ImageFragment;
import com.kkrasylnykov.l19_fileandpermisionexample.Model.FileOnServer;
import com.kkrasylnykov.l19_fileandpermisionexample.R;

public class ViewActivity extends AppCompatActivity {
    public final static String KEY_FILE_PATHS = "KEY_FILE_PATHS";
    public final static String KEY_POSITION = "KEY_POSITION";

    FileOnServer[] m_arrFiles = null;

    ViewPager m_ViewPager = null;
    ImageViewFragmentAdapter m_adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        m_ViewPager = (ViewPager) findViewById(R.id.ViewPager);

        int nPosition = 0;

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                nPosition = bundle.getInt(KEY_POSITION);
                m_arrFiles = new FileOnServer[MainActivity.getFiles().size()];
                MainActivity.getFiles().toArray(m_arrFiles);
            }
        }
        m_adapter = new ImageViewFragmentAdapter(getSupportFragmentManager(),m_arrFiles);
        m_ViewPager.setAdapter(m_adapter);
        m_ViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getCurrentPage().changeCountShowImage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        m_ViewPager.setCurrentItem(nPosition);
    }

    public ImageFragment getCurrentPage(){
        ImageFragment returnPage = (ImageFragment) m_adapter.instantiateItem(m_ViewPager, m_ViewPager.getCurrentItem());
        return returnPage;
    }
}
