package com.kkrasylnykov.l26_externalcallcontentproviderexample;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String AUTHORITY = "com.kkrasylnykov.l1989_fileandpermisionexample.ContentProviders.SettingsContentProvider";
    public static final String COUNT_SHOW_FILES     = "count_show_files";
    public static final Uri COUNT_SHOW_FILES_URI    = Uri.parse("content://" + AUTHORITY + "/" + COUNT_SHOW_FILES);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = (TextView) findViewById(R.id.textview);
        String strData = "";
        Cursor cursor = null;
        Uri uri = COUNT_SHOW_FILES_URI;
        cursor = getContentResolver().query(uri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst()){
           do{
               String strName = cursor.getString(0);
               int nCount = cursor.getInt(1);
               strData += strName + " -> " + nCount + "\n";
           } while (cursor.moveToNext());
        } else {
            strData = "Error!!!";
        }
        if(cursor != null){
            cursor.close();
        }

        textView.setText(strData);


    }
}
