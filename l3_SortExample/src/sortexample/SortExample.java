
package sortexample;

import java.util.Scanner;

public class SortExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int nCount = -1;
        
        System.out.print("Введите колличество эллементов: ");
        nCount = scan.nextInt();
        
        int[] arrForSort = new int[nCount];
        
        for(int i=0; i<arrForSort.length;i++){
            int nNum = i +1;
            System.out.print("Введите " + nNum + " эллемент: ");
            arrForSort[i] = scan.nextInt();
        }
        
        boolean bFlag = false;
        int nDelta = 1;
        
        do{
            bFlag = false;
            for(int i=0; i<arrForSort.length-nDelta;i++){
                if(arrForSort[i]<arrForSort[i+1]){
                    bFlag = true;
                    int nTmp = arrForSort[i];
                    arrForSort[i] = arrForSort[i+1];
                    arrForSort[i+1] = nTmp;
                }
            }   
            nDelta++;
        } while(bFlag);
        
        System.out.print("[");
        for(int nVal:arrForSort){
            System.out.print(nVal + ", ");
        }
        System.out.println("]");
    }
    
}
