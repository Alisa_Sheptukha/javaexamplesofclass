
package javaapplication6;

import java.util.ArrayList;
import java.util.Scanner;

public class JavaApplication6 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        ArrayList<Float> arr = new ArrayList<>();
        
        Float fVal = -1f;
        
        do{
            fVal = scan.nextFloat();
            if(fVal>0){
                arr.add(fVal);
            } else {
                break;
            }
            
        }while(true);
        Float fSum = 0f;
        for (int i=0; i< arr.size(); i++){
            fSum+=arr.get(i);
        }
        
        System.out.println("fSum -> " + fSum);
    }
    
}
