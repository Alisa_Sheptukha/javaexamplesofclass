
package l5_oopexample;

public class BaseClass {
    
    private int m_nCountPaws = 0;
    private int m_nType = 0;
    private String m_strClassName = "";
    private boolean m_bIsDayTime = true;
    
    public BaseClass(){
        setCountPaws(0);
        setType(0);
        setClassName("");
        setIsDayTime(true);
    }
    
    public BaseClass(String strClassName){
        setClassName(strClassName);
        
        setCountPaws(0);
        setType(0);
        setIsDayTime(true);
    }
    
    public BaseClass(String strClassName, int nType){
        setClassName(strClassName);
        setType(nType);
        
        setCountPaws(0);
        setIsDayTime(true);
    }
    
    public int getCountPaws(){
        return m_nCountPaws;
    }
    
    public int getType(){
        return m_nType;
    }
    
    public String getClassName(){
        return m_strClassName;
    }
    
    public boolean isDayTime(){
        return m_bIsDayTime;
    }
    
    
    public void setCountPaws(int nCountPaws){
        m_nCountPaws = nCountPaws;
    }
    
    public void setType(int nType){
        this.m_nType = nType;
    }
    
    public void setClassName(String strClassName){
        m_strClassName = strClassName;
    }
    
    public void setIsDayTime(boolean bIsDayTime){
        m_bIsDayTime = bIsDayTime;
    }
}
