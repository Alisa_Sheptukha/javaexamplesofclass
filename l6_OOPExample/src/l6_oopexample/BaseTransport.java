package l6_oopexample;

public abstract class BaseTransport {
    
    public static final String TYPE_CAR = "TYPE_CAR";
    public static final String TYPE_MOTO = "TYPE_MOTO";
    
    public static final int TYPE_FUEL_GAS = 1;
    public static final int TYPE_FUEL_DIESEL = 2;
    public static final int TYPE_FUEL_PETROL = 3;
    
    private String m_strColor = "";
    private float m_fPowerD = 0.0f;
    private int m_nDWheel = 0;
    private int m_nCountWheel = 0;
    private int m_nTypeFuel = TYPE_FUEL_PETROL;
    private String m_strCompanyName = "";
    
    private String m_strType = "";
    
    public BaseTransport(String strType){
        setColor("");
        setPowerD(0.0f);
        setDWheel(0);
        setCountWheel(0);
        setTypeFuel(TYPE_FUEL_PETROL);
        setCompanyName("");
        m_strType = strType;
    }
    
    public abstract void insertData();
    public abstract void outData();
    
    public boolean isSearch(String strSearch){
        return m_strCompanyName.contains(strSearch);
    }
    
    public String getColor(){
        return m_strColor;
    }
    
    public float getPowerD(){
        return m_fPowerD;
    }
    
    public int getDWheel(){
        return m_nDWheel;
    }
    
    public int getCountWheel(){
        return m_nCountWheel;
    }
    
    public int getTypeFuel(){
        return m_nTypeFuel;
    }
    
    public String getCompanyName(){
        return m_strCompanyName;
    }
    
    public String getType(){
        return m_strType;
    }
    
    public void setColor(String strColor){
        m_strColor = strColor;
    }
    
    public void setPowerD(float fPowerD){
        m_fPowerD = fPowerD;
    }
    
    public void setDWheel(int nDWheel){
        m_nDWheel = nDWheel;
    }
    
    public void setCountWheel(int nCountWheel){
        m_nCountWheel = nCountWheel;
    }
    
    public void setTypeFuel(int nTypeFuel){
        m_nTypeFuel = nTypeFuel;
    }
    
    public void setCompanyName(String strCompanyName){
        m_strCompanyName = strCompanyName;
    }
}
