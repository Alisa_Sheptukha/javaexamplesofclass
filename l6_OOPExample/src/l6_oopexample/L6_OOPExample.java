package l6_oopexample;

import java.util.ArrayList;
import java.util.Scanner;

public class L6_OOPExample {

    public static void main(String[] args) {
        ArrayList<BaseTransport> arrData = new ArrayList<>();
        
        Scanner scan = new Scanner(System.in);
        int nCase = 0;
        do{
            System.out.println("Выберите пунк меню:\n1. Добавить новый элемент"+
                    "\n2. Поиск\n3. Просмотреть весь список\n0. Выход");
            nCase = scan.nextInt();
            if (nCase==1){
                System.out.println("Какое транспортное средство вы хотите добавить:\n1. Автомобиль"+
                    "\n2. Мотоцыкл");
                nCase = scan.nextInt();
                BaseTransport item = null;
                if (nCase==1){
                    item = new Car();
                }else if (nCase==2){
                    item = new Moto();
                }
                item.insertData();
                arrData.add(item);
            } else if (nCase==2){
                System.out.println("Введите стоку поиска: ");
                scan.nextLine();
                String strSerch = scan.nextLine();
                for (BaseTransport item:arrData){
                    if (item.isSearch(strSerch)){
                        item.outData();
                    }
                    
                }
            } else if (nCase==3){
                for (BaseTransport item:arrData){
                    item.outData();
                }
                
            } else if (nCase==0){
                break;
            }
        } while (true);
    }
    
}
