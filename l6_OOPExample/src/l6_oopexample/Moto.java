
package l6_oopexample;

import java.util.Scanner;

public class Moto extends BaseTransport{

    public Moto() {
        super(TYPE_MOTO);
    }

    @Override
    public void insertData() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите название производителя мотоцыкла:");
        String strCompanyName = scan.nextLine();
        System.out.print("Введите цвет мотоцыкла:");
        String strColor = scan.nextLine();
        System.out.print("Введите мощность двигателя мотоцыкла:");
        float fDPower = scan.nextFloat();
        System.out.print("Введите кол-во колес мотоцыкла:");
        int nCountWheel = scan.nextInt();
        System.out.print("Введите диаметр колес мотоцыкла:");
        int nDWheel = scan.nextInt();
        int nTypeFuel = TYPE_FUEL_PETROL;
        
        setCompanyName(strCompanyName);
        setColor(strColor);
        setPowerD(fDPower);
        setCountWheel(nCountWheel);
        setDWheel(nDWheel);
        setTypeFuel(nTypeFuel);
    }

    @Override
    public void outData() {
        System.out.print("Мотоцикл производства " + getCompanyName() + ", мощность " + getPowerD() + " Ватт, цвет "
                + getColor() + ", " + getCountWheel() + " колес диаметром" + getDWheel());
    }
    
}
