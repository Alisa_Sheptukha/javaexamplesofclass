package com.kkrasylnykov.l7_firstapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    public  static final String KEY_PASSWORD = "KEY_PASSWORD";

    public  static final String KEY_NAME = "KEY_NAME";

    TextView m_textView = null;
    EditText m_editText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        m_textView = (TextView)  findViewById(R.id.textViewSecondActivity);

        m_editText = (EditText) findViewById(R.id.editTextSecondActivity);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle!=null){
            String strText = bundle.getString(KEY_PASSWORD, "ERROR");
            m_textView.setText("Пароль: " + strText);
        }

        Button btnOk = (Button) findViewById(R.id.okButtonSecondActivity);
        btnOk.setOnClickListener(this);

        Button btnCancel = (Button) findViewById(R.id.cancelButtonSecondActivity);
        btnCancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.okButtonSecondActivity:
                setResult(RESULT_OK);
                break;
            case R.id.cancelButtonSecondActivity:
                String strText = m_editText.getText().toString();
                Intent intent = new Intent();
                intent.putExtra(KEY_NAME, strText);
                setResult(RESULT_CANCELED, intent);
                //setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }

    @Override
    public void onBackPressed() {
    }
}
